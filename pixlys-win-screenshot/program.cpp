#include <windows.h>
#include <stdio.h>
#include <gdiplus.h>

#pragma comment(lib, "gdiplus.lib")

typedef struct {
	INT x;
	INT y;
	INT w;
	INT h;
} P_AREA;

const char g_windowClassName[] = "pixlys";
const char g_selectionWindowClassName[] = "sel_pixlys";
const int g_selectionSizePadding = 3;

P_AREA g_selArea = { 0 };
POINT g_startingPoint = { 0 };
POINT g_draggingPoint = { 0 };
BOOL g_hasStartingPoint = FALSE;
BOOL g_hasMoved = FALSE;
HBRUSH g_selBrush;
HPEN g_selPen;
HFONT g_selFont;
HWND g_selHwnd; // Reference to selection window

// Command line options
WCHAR* g_targetPath;
BOOL g_fullscreen = FALSE;

using namespace Gdiplus;
using namespace std;

void GetSelectionArea(P_AREA* a,
					  POINT* startingPoint,
					  POINT* draggingPoint)
{
	// Negative direction on X
	if (startingPoint->x > draggingPoint->x) {
		a->x = draggingPoint->x;
		a->w = startingPoint->x - draggingPoint->x;
	}
	else // Normal direction on X
	{
		a->x = startingPoint->x;
		a->w = draggingPoint->x - startingPoint->x;
	}

	// Negative direction on Y
	if (startingPoint->y > draggingPoint->y) {
		a->y = draggingPoint->y;
		a->h = startingPoint->y - draggingPoint->y;
	}
	else // Normal direction on Y
	{
		a->y = startingPoint->y;
		a->h = draggingPoint->y - startingPoint->y;
	}
}

BOOL GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
   UINT num = 0; // Number of image encoders
   UINT size = 0; // Size of the image encoder array in bytes
   ImageCodecInfo* pImageCodecInfo = NULL;

   GetImageEncodersSize(&num, &size);

   if (size == 0)
   {
	   return FALSE; // Failure
   }

   pImageCodecInfo = (ImageCodecInfo*)(malloc(size));

   if (pImageCodecInfo == NULL)
   {
	   return FALSE; // Failure
   }

   GetImageEncoders(num, size, pImageCodecInfo);

   for (UINT j = 0; j < num; j++)
   {
      if (wcscmp(pImageCodecInfo[j].MimeType, format) == 0)
	  {
         *pClsid = pImageCodecInfo[j].Clsid;
         free(pImageCodecInfo);
         return j; // Found encoder
      }
   }

   free(pImageCodecInfo);
   return FALSE; // Failure
}

BOOL DoScreenshot(HWND bgHwnd, P_AREA* area)
{
	HDC sourceHdc = GetDC(bgHwnd);
	// Creates a copy of the same device context kind as source
	HDC targetHdc = CreateCompatibleDC(sourceHdc);
	HBITMAP targetBmp = CreateCompatibleBitmap(sourceHdc, area->w, area->h);

	SelectObject(targetHdc, targetBmp);

	// Copy color-data from source to destination
	BitBlt(
		targetHdc, // Where to copy to
		0, 0, // x, y destination rectangle coordinates
		area->w, area->h, // w, h src and dest rectangle size
		sourceHdc, // Where to copy from
		area->x, area->y, // x, y source rectangle coordinates
		SRCCOPY // Raster-operation code, just copy in our case
	);

	// The bitmap now contains the image data. We don't want a BMP image
	// though, so we need to encode it into PNG. We do this using GDI+.
	GdiplusStartupInput	gdiplusStartupInput;
	ULONG_PTR gdiplusToken;
	CLSID clsidEncoder;

	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	Bitmap *bitmapObj = new Bitmap(targetBmp, NULL);

	if (!GetEncoderClsid(L"image/png", &clsidEncoder))
	{
		return FALSE;
	}

	if(bitmapObj->Save((WCHAR*)g_targetPath, &clsidEncoder, NULL) != 0)
	{
		return FALSE;
	}

	delete bitmapObj;
	GdiplusShutdown(gdiplusToken);
	return TRUE;
}

/**
 * Selection window procedure
 */
LRESULT CALLBACK SelWndProc(HWND _selHwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	// Paint on the selection window when it's resized
	if (msg == WM_ERASEBKGND)
	{
		HDC hdc = GetDC(_selHwnd);

		SelectObject(hdc, g_selBrush);
		SelectObject(hdc, g_selPen);
		SelectObject(hdc, g_selFont);

		Rectangle(
			hdc,
			0, 0, // x, y
			g_selArea.w, g_selArea.h // width, height
		);

		// Construct the size string to be shown in the corner
		char sizeStr[10];
		float charWidth = 6.4F; // Represents char width (px) at font size 13

		sprintf_s(sizeStr, sizeof(sizeStr), "%dx%d", g_selArea.w, g_selArea.h);

		int xOffset = (int)((charWidth * strlen(sizeStr)) + g_selectionSizePadding);
		int yOffset = 14 + g_selectionSizePadding; // 14 = Font size + 1
		int xPos = g_selArea.w - xOffset;
		int yPos = g_selArea.h - yOffset;

		// Make sure that it fits
		if (xPos > 1 && yPos > 1)
		{
			TextOut(hdc, xPos, yPos, sizeStr, strlen(sizeStr));
		}

		ReleaseDC(_selHwnd, hdc);
		return TRUE;
	}
	else
	{
		return DefWindowProc(_selHwnd, msg, wParam, lParam);
	}
}

/**
 * Background window procedure
 */
LRESULT CALLBACK WndProc(HWND bgHwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg) {
		case WM_CLOSE:
		case WM_RBUTTONDOWN: // Abort on right click or window close (Alt+F4)
			DestroyWindow(bgHwnd);
			break;
		case WM_TIMER: // Abort on Escape
			if (GetKeyState(VK_ESCAPE) & 0x8000)
			{
				DestroyWindow(bgHwnd);
			}
			break;
		case WM_LBUTTONDOWN:
			if (!g_hasStartingPoint)
			{
				// Capture mouse cursor. This is essential. If we aren't
				// capturing the cursor on the background window, it's
				// captured by the selection window. If the cursor slips
				// outside the window (which it will), we can no longer
				// capture any move events.
				SetCapture(bgHwnd);

				// Capture starting point
				g_startingPoint.x = LOWORD(lParam);
				g_startingPoint.y = HIWORD(lParam);
				g_hasStartingPoint = TRUE;

				// Show the selection window
				ShowWindow(g_selHwnd, SW_SHOW);
			}
			else
			{
				// What?
			}
			break;
		case WM_LBUTTONUP:
			ReleaseCapture(); // Release mouse cursor capture

			if (g_hasStartingPoint && g_hasMoved)
			{
				// Hide the selection window
				ShowWindow(g_selHwnd, SW_HIDE);

				// Take screenshot
				DoScreenshot(bgHwnd, &g_selArea);
			}

			// Clean up and die
			DestroyWindow(bgHwnd);
			break;
		case WM_MOUSEMOVE:
			if (g_hasStartingPoint)
			{
				g_draggingPoint.x = LOWORD(lParam);
				g_draggingPoint.y = HIWORD(lParam);
				g_hasMoved = TRUE;

				GetSelectionArea(&g_selArea, &g_startingPoint, &g_draggingPoint);

				// Move selection window
				MoveWindow(
					g_selHwnd,
					g_selArea.x, g_selArea.y,
					g_selArea.w, g_selArea.h,
					TRUE
				);
			}

			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			// Bubble up the event
			return DefWindowProc(bgHwnd, msg, wParam, lParam);
	}

	return 0;
}

BOOL ParseOptions()
{
	int argc = 0;
	LPWSTR* argv;

	argv = CommandLineToArgvW(GetCommandLineW(), &argc);
	
	if (argv == NULL)
	{
		return FALSE; // Could not parse command line
	}

	if (argc < 2)
	{
		return FALSE; // Command line options are required
	}

	if (wcscmp(argv[1], L"--fullscreen") == 0)
	{
		// This argument cannot be the last one
		if (argc == 2)
		{
			return FALSE;
		}

		g_fullscreen = TRUE;
	}

	// Last argument is the destination path
	WCHAR* path = argv[argc - 1];
	size_t stringLen = wcslen(path) + 1;
	size_t allocBytes = sizeof(WCHAR) * stringLen;
	g_targetPath = (WCHAR*)malloc(allocBytes);
	wcsncpy_s(g_targetPath, stringLen, path, allocBytes);

	LocalFree(argv);

	return TRUE;
}

/**
 * Main application entrypoint
 */
int WINAPI WinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow)
{
	WNDCLASSEX wc; // Window class
	HWND bgHwnd; // Handle to transparent background window
	MSG Msg; // Structure to hold a message
	P_AREA screen; // Structure to hold virtual screen size

	if (ParseOptions() == FALSE)
	{
		// TODO: Print help
		return 1;
	}

	// Get the virtual screen position and size
	screen.x = GetSystemMetrics(SM_XVIRTUALSCREEN);
	screen.y = GetSystemMetrics(SM_YVIRTUALSCREEN);
	screen.w = GetSystemMetrics(SM_CXVIRTUALSCREEN);
	screen.h = GetSystemMetrics(SM_CYVIRTUALSCREEN);

	// Size of the structure
	wc.cbSize = sizeof(WNDCLASSEX);
	// Window class style
	wc.style = 0;
	// Connect window procedure callback
	wc.lpfnWndProc = WndProc;
	// Amount of extra data allocated for this class in memory
	wc.cbClsExtra = 0;
	// Amount of extra data allocated in memory per window of this type
	wc.cbWndExtra = 0;
	// Handle to the instance that contains the window procedure for the class
	wc.hInstance = hInstance;
	// Choose which cursor to use (do not load existing)
	wc.hCursor = LoadCursor(NULL, IDC_CROSS);
	// Brush handle to use for window class background
	wc.hbrBackground = 0;
	// Resource name of the class menu (we dont want any)
	wc.lpszMenuName = NULL;
	// Name of the window class
	wc.lpszClassName = g_windowClassName;
	// Icon to use for window class
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&wc))
	{
		MessageBox(NULL, "Base window registration failed", "Error",
			MB_ICONEXCLAMATION | MB_OK);
		return 1;
	}

	// Change some properties and create another class for the selection window.

	// Make the window redraw when it's resized
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = SelWndProc;
	wc.hbrBackground = (HBRUSH)GetStockObject(DKGRAY_BRUSH);
	wc.lpszClassName = g_selectionWindowClassName;

	if (!RegisterClassEx(&wc))
	{
		MessageBox(NULL, "Selection window registration failed", "Error",
			MB_ICONEXCLAMATION | MB_OK);
		return 1;
	}

	// Create the transparent "base" window that covers the entire screen
	bgHwnd = CreateWindowEx(
		// Extended window style
		WS_EX_TRANSPARENT | WS_EX_TOOLWINDOW | WS_EX_TOPMOST | WS_EX_NOACTIVATE,
		g_windowClassName,
		NULL, // Window name (we dont want any)
		WS_POPUP, // Cover the entire screen
		0, 0, // x, y position
		0, 0, // width, height
		NULL, // We have no parent since this is the main window
		NULL,
		hInstance,
		NULL
	);

	// Move to cover entire screen
	MoveWindow(bgHwnd, screen.x, screen.y, screen.w, screen.h, TRUE);

	if (bgHwnd == NULL)
	{
		MessageBox(NULL, "Window creation failed", "Error",
			MB_ICONEXCLAMATION | MB_OK);
		return 2;
	}

	// If we should take a screen shot of the entire screen, we do not need to
	// init selection window. We simply take a screenshot with the screen as
	// area and call it a day.
	if (g_fullscreen)
	{
		ShowWindow(bgHwnd, SW_SHOW);
		return DoScreenshot(bgHwnd, &screen) ? 0 : 1;
	}

	// Create the transparent "base" window that covers the entire screen
	g_selHwnd = CreateWindowEx(
		// Extended window style
		WS_EX_TOOLWINDOW | WS_EX_LAYERED | WS_EX_NOACTIVATE,
		g_selectionWindowClassName,
		NULL, // Window name (we dont want any)
		WS_POPUP, // Cover the entire screen
		0, 0, // x, y position
		0, 0, // width, height
		bgHwnd, // Parent window
		NULL,
		hInstance,
		NULL
	);

	if (g_selHwnd == NULL)
	{
		MessageBox(NULL, "Seletion window creation failed", "Error",
			MB_ICONEXCLAMATION | MB_OK);
		return 2;
	}

	SetLayeredWindowAttributes(
		g_selHwnd,
		RGB(255, 0, 0),
		140, // Transparency
		LWA_COLORKEY | LWA_ALPHA
	);

	// Init paint tools
	g_selBrush = CreateSolidBrush(RGB(255, 255, 255));
	g_selPen = CreatePen(PS_SOLID, 1, RGB(80, 80, 80));
	g_selFont = CreateFont(13, 0, 0, 0, FW_REGULAR,
		FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS, PROOF_QUALITY, FIXED_PITCH | FF_MODERN,
		"Consolas");

	ShowWindow(bgHwnd, nCmdShow);
	UpdateWindow(bgHwnd);

	// Quite ugly way of polling escape key state. This could definitely be
	// improved.
	SetTimer(bgHwnd, 1, 75, NULL);

	// Enter the message loop. GetMessage will retrieve messages for any window
	// that belongs to the current thread, and any messages on the current
	// threads message queue whose hwnd is NULL.
	while (GetMessage(&Msg, NULL, 0, 0) > 0)
	{
		// Translate virtual-key messages into character messages
		TranslateMessage(&Msg);

		// Dispatch a message to a window procedure
		DispatchMessage(&Msg);
	}

	// Play nice and free up some memory
	DeleteObject(g_selBrush);
	DeleteObject(g_selPen);
	DeleteObject(g_selFont);

	return Msg.wParam; // Exit with code from procedure
}